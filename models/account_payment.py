from odoo import _, api, fields, models
from datetime import datetime


class AccountPayment(models.Model):
    _inherit = "account.payment"

    cancelation_reason = fields.Selection(
        selection=[
            ("01", "01 Comprobante emitido con errores con relaciòn"),
            ("02", "02 Comprobante emitido con errores sin relacion"),
            ("03", "03 No se llevo acabo la operacion"),
            ("04", "04 Operacion nominativa relacionada en una factura global"),
        ],
        string="Motivo de Cancelación",
        copy=False,
    )
    replacement_invoice_id = fields.Many2one(
        comodel_name="account.payment",
        string="Factura de remplazo",
        copy=False,
    )
    uuid_replacemnt = fields.Char(
        string="UUID Remplazo",
        readonly=True,
        compute="_compute_uuid_replacement",
        copy=False,
    )

    @api.onchange("replacement_invoice_id, cancelation_reason")
    def _compute_uuid_replacement(self):
        for record in self:
            record.uuid_replacemnt = 0
            if record.cancelation_reason == "01":
                record.uuid_replacemnt = (
                    record.replacement_invoice_id.l10n_mx_edi_cfdi_uuid
                )
            else:
                record.uuid_replacemnt = 0
