from odoo import _, api, fields, models, tools
from datetime import datetime
import requests
from lxml.objectify import fromstring
from odoo.tools.float_utils import float_repr

CFDI_SAT_QR_STATE = {
    "No Encontrado": "not_found",
    "Cancelado": "cancelled",
    "Vigente": "valid",
}


class AccountMove(models.Model):
    _inherit = "account.move"

    cancelation_reason = fields.Selection(
        selection=[
            ("01", "01 Comprobante emitido con errores con relaciòn"),
            ("02", "02 Comprobante emitido con errores sin relacion"),
            ("03", "03 No se llevo acabo la operacion"),
            ("04", "04 Operacion nominativa relacionada en una factura global"),
        ],
        string="Motivo de Cancelación",
        copy=False,
    )
    replacement_invoice_id = fields.Many2one(
        comodel_name="account.move",
        string="Factura de remplazo",
        copy=False,
    )
    uuid_replacemnt = fields.Char(
        string="UUID Remplazo",
        readonly=True,
        compute="_compute_uuid_replacement",
        copy=False,
    )
    to_cancel_invoice = fields.Boolean(
        readonly=True,
        copy=False,
    )

    def cancelation_request_create(self):
        for record in self:
            res = super(AccountMove, self).cancelation_request_create()
            record.to_cancel_invoice = True
            return res

    @api.onchange("replacement_invoice_id, cancelation_reason")
    def _compute_uuid_replacement(self):
        for record in self:
            record.uuid_replacemnt = 0
            if record.cancelation_reason == "01":
                record.uuid_replacemnt = (
                    record.replacement_invoice_id.l10n_mx_edi_cfdi_uuid
                )
            else:
                record.uuid_replacemnt = 0

    def l10n_mx_edi_update_sat_status(self):
        """Synchronize both systems: Odoo & SAT to make sure the invoice is valid."""
        url = "https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc?wsdl"
        headers = {
            "SOAPAction": "http://tempuri.org/IConsultaCFDIService/Consulta",
            "Content-Type": "text/xml; charset=utf-8",
        }
        template = """<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:ns0="http://tempuri.org/" xmlns:ns1="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
   <SOAP-ENV:Header/>
   <ns1:Body>
      <ns0:Consulta>
         <ns0:expresionImpresa>${data}</ns0:expresionImpresa>
      </ns0:Consulta>
   </ns1:Body>
</SOAP-ENV:Envelope>"""
        namespace = {
            "a": "http://schemas.datacontract.org/2004/07/Sat.Cfdi.Negocio.ConsultaCfdi.Servicio"
        }
        for inv in self.filtered("l10n_mx_edi_cfdi"):
            supplier_rfc = inv.l10n_mx_edi_cfdi_supplier_rfc
            customer_rfc = inv.l10n_mx_edi_cfdi_customer_rfc
            total = float_repr(
                inv.l10n_mx_edi_cfdi_amount,
                precision_digits=inv.currency_id.decimal_places,
            )
            uuid = inv.l10n_mx_edi_cfdi_uuid
            motivo = inv.cancelation_reason
            if inv.uuid_replacemnt and inv.cancelation_reason == "01":
                FolioSustitucion = inv.uuid_replacemnt
                params = "?re=%s&amp;rr=%s&amp;tt=%s&amp;id=%s&amp;motivo=%s&amp;FolioSustitucion=%s" % (
                    tools.html_escape(tools.html_escape(supplier_rfc or "")),
                    tools.html_escape(tools.html_escape(customer_rfc or "")),
                    total or 0.0,
                    uuid or "",
                    motivo or "",
                    FolioSustitucion or "",
                )

            params = "?re=%s&amp;rr=%s&amp;tt=%s&amp;id=%s&amp;motivo=%s" % (
                tools.html_escape(tools.html_escape(supplier_rfc or "")),
                tools.html_escape(tools.html_escape(customer_rfc or "")),
                total or 0.0,
                uuid or "",
                motivo or "",
            )
            soap_env = template.format(data=params)
            try:
                soap_xml = requests.post(
                    url, data=soap_env, headers=headers, timeout=20
                )
                response = fromstring(soap_xml.text)
                status = response.xpath("//a:Estado", namespaces=namespace)
            except Exception as e:
                inv.l10n_mx_edi_log_error(str(e))
                continue
            inv.l10n_mx_edi_sat_status = CFDI_SAT_QR_STATE.get(
                status[0] if status else "", "none"
            )
