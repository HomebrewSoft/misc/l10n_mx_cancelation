{
    "name": "l10n_mx_cancelation",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "account",
        "l10n_mx_edi",
    ],
    "data": [
        # security
        # data
        # reports
        # views
        "views/account_move.xml",
        "views/account_payment.xml",
    ],
}
